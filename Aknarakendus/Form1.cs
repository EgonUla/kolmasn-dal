﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace Aknarakendus
{
    public partial class Form1 : Form

        
        
    {


        List<Tulemus> Tulemused = null;

        string AllItems = "All";





public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {



        }

        private void Form1_Load(object sender, EventArgs e)
        {

            Tulemused =
                System.IO.File.ReadAllLines("..\\..\\Protokoll.txt")
                .Select(x => x.Split(','))
                .Select(x => new Tulemus
                {

                    Nimi = x[0],
                    Distants = double.TryParse(x[1], out double d) ? d : 0,
                    Aeg = double.TryParse(x[2], out double a) ? a : 0,
                })

            .ToList();
            this.dataGridView1.DataSource = Tulemused;
            this.dataGridView1.Columns[3].DefaultCellStyle.Format = "F2" ;
            //this.dataGridView1.Columns[3].DefaultCellStyle.Alignment = "F2" ;

            this.comboBox1.DataSource =
              Tulemused.Select(x => x.Nimi).Distinct()
              .Union(new string[] { AllItems})
              .OrderBy(x => x == AllItems ? "" : x)
              .ToList();


        }

        private void button1_Click(object sender, EventArgs e)
        {


            




        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
           
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Nimi = this.comboBox1.SelectedItem.ToString();
            this.dataGridView1.DataSource = Tulemused
                .Where(x => x.Nimi == Nimi || Nimi == AllItems)
                .ToList();




        }
    }
}
