﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EntityÜlesanne
{
    public partial class Form1 : Form
    {

        NorthwindEntities db = new NorthwindEntities();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.comboBox1.DataSource = db.Customers
                .Select(x => x.Country)
                .ToList();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = db.Customers.Where(c => c.Country == this.comboBox1.SelectedItem)
                //.SingleOrDefault().Region
                //.Select(x => x.ProductID, x.ProductName x.UnitPrice, x.UnitStock )
                .ToList();
        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            new Form1().Show();
        }
    }
}
