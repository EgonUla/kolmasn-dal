﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AkenEntity
{
    public partial class Form1 : Form
    {
        // kui static siis kõigil akendel seda tüüpi, kõigil ühine db
        // Kui MITTE static siis kõigil akendel eraldi db
        NorthwindEntities db = new NorthwindEntities();


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.comboBox1.DataSource = db.Categories
                .Select(x =>  x.CategoryName )
                .ToList();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = db.Categories.Where(c => c.CategoryName == this.comboBox1.SelectedItem)
                .SingleOrDefault().Products
                //.Select(x => x.ProductID, x.ProductName x.UnitPrice, x.UnitStock )
                .ToList();

            this.dataGridView1.Columns["CategoryID"].Visible = false;
            this.dataGridView1.Columns["Discontinued"].Visible = false;
            
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            db.SaveChanges();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            new Form1().Show();
        }
    }
}
