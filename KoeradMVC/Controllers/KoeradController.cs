﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Newtonsoft.Json;
using KoeradMVC.Models;

namespace KoeradMVC.Controllers
{
    public class KoeradController : Controller
    {
        const string KoeradFile = @"~\Content\Koearad.json";
        public KoeradController()
        {
            JsonConvert.DeserializeObject<List<Koer>>()
        }
        // GET: Koerad
        public ActionResult Index()
        {
            return View();
        }

        // GET: Koerad/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Koerad/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Koerad/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Koerad/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Koerad/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Koerad/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Koerad/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
