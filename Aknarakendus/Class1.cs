﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aknarakendus
{
    class Tulemus
    {
        public string Nimi { get; set; }
        public double Distants { get; set; }
        public double Aeg { get; set; }
        public double Kiirus => Distants / Aeg; 

    }
}
