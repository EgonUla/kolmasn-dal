﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KoeradMVC.Models
{
    public class Koer
    {
        public static List<Koer> Koerad = new List<Koer>();
        static int nr = 0;

        public int Id { get; set; }
        public string Nimi { get; set; }
        public string Värv { get; set; }

        public static Koer Find(int id) => Koerad.Where(x => x.Id == id).SingleOrDefault();
    }
}