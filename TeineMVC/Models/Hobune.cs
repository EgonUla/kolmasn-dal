﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeineMVC.Models
{
    public class Hobune
    {
        static Dictionary<int, Hobune> _Hobused = new Dictionary<int, Hobune>();

        static Hobune() // Static constructor
        {
            new Hobune { Nimi = "Kevin", Värv = "Kõrb" }; 
            new Hobune { Nimi = "Verde", Värv = "Kõrb" }; 
        }

        public string Nimi { get; set; }
        public int Id { get; set; } //= nr++;
        static int nr = 0;
        public string Värv { get; set; }

        public Hobune() { _Hobused.Add(this.Id= ++nr, this); }

        public static Hobune Find(int id) =>
            _Hobused.ContainsKey(id) ? _Hobused[id] : null;

        public static IEnumerable<Hobune> Hobused => _Hobused.Values;



    }
}